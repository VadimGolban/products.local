<?php

namespace Application\Form;

use Application\Entity\Category;
use Zend\Form\Form;

class ProductFilterForm extends Form
{

    public function __construct($categories)
    {
        foreach($categories as $category) {
            /** @var $category Category **/
            $this->categories[$category->getId()] = $category->getTitle();
        }

        parent::__construct('category');
        $this->prepareElements();
    }

    public function prepareElements()
    {   $this->add(array(
        'name' => 'category',
        'type' => 'Zend\Form\Element\Select',
        'attributes' =>  array(
            'id' => 'category',
            'options' => $this->categories,
        ),
        'options' => array(
            'label' => 'Category ',
        ),
    ));


        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}