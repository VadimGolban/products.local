<?php

namespace Application\Form;

use Application\Entity\Category;
use Zend\Form\Form;

class ProductForm extends Form
{
    private $categories = [];

    public function __construct($categories)
    {
        foreach($categories as $category) {
            /** @var $category Category **/
            $this->categories[$category->getId()] = $category->getTitle();
        }

        parent::__construct('product');
        $this->prepareElements();
    }
    public function prepareElements(){

        $this->add(array(
            'name' => 'category',
            'type' => 'Zend\Form\Element\Select',
            'attributes' =>  array(
                'id' => 'category',
                'options' => $this->categories,
            ),
            'options' => array(
                'label' => 'Category ',
            ),
        ));

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'title',
            'type' => 'Text',
            'options' => array(
                'label' => 'Name',
            ),
            'attributes' => [
                'class' => 'product-text',
                'required' => true,
            ],
        ));
        $this->add(array(
            'name' => 'description',
            'type' => 'Text',
            'options' => array(
                'label' => 'Description',
            ),
            'attributes' => [
                'class' => 'product-text',
                'required' => true,
            ],
        ));
        $this->add(array(
            'name' => 'price',
            'type' => 'Text',
            'options' => array(
                'label' => 'Price',
            ),
            'attributes' => [
                'class' => 'product-price',
                'required' => true,
            ],
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}