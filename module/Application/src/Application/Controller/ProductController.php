<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Entity\Category;
use Application\Entity\Product;
use Application\Form\ProductForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ProductController extends AbstractActionController
{
    public function createAction()
    {
        /** @var \Doctrine\ORM\EntityManager $category */
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $categories = $em->getRepository('Application\Entity\Category')->findAll();

        $form = new ProductForm($categories);
        $request = $this->getRequest();

        if ($request->isPost()) {
            $product = new Product();
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();

                /** @var $category Category */
                if (!$category = $em->getRepository('Application\Entity\Category')->find($data['category'])) {
                    throw new \Exception('Category not found');
                }

                $product->setCategory($category);

                $product->setTitle($data['title']);
                $product->setDescription($data['description']);
                $product->setPrice($data['price']);

                $em->persist($product);
                $em->flush();


                return $this->redirect()->toRoute('products', ['action' => 'index']);
            }
        }

        return array('form' => $form);
    }

    public function indexAction()
    {
        /** @var EntityManager $em */
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $products = $em->getRepository('Application\Entity\Product')->findAll();

        return new ViewModel(['products' => $products]);
    }

    public function editAction()
    {
        /** @var Product $product */
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $product = $em->getRepository('Application\Entity\Product')->find($productId = $this->params()->fromQuery('id', 0));
        $categories = $em->getRepository('Application\Entity\Category')->findAll();

        $form = new ProductForm($categories);
        $request = $this->getRequest();

        $form->populateValues($product->toArray());

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = $form->getData();

                /** @var $category Category */
                if (!$category = $em->getRepository('Application\Entity\Category')->find($data['category'])) {
                    throw new \Exception('Category not found');
                }

                $product->setCategory($category);

                $product->setTitle($data['title']);
                $product->setDescription($data['description']);
                $product->setPrice($data['price']);

                $em->persist($product);
                $em->flush();

                return $this->redirect()->toRoute('products', ['action' => 'index']);
            }
        }

        return array('form' => $form);
    }

    public function deleteAction()
    {
         /** @var Product $deleteProduct */
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $deleteProduct = $em->getRepository('Application\Entity\Product')->find($productId = $this->params()->fromQuery('id', 0));
        if ($deleteProduct ) {
            $em->remove($deleteProduct);
            $em->flush();
        }

        return $this->redirect()->toRoute('products', ['action' => 'index']);
    }


    public function carouselAction(){

        return new ViewModel();
    }
}
