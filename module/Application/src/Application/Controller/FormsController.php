<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\Captcha;
use Zend\Json\Server\Response;
use Zend\View\Model\JsonModel;



class FormsController extends AbstractActionController
{

    public function indexAction()
    {

           return new viewModel();
    }

    public  function ajaxSecondAction(){

        $object = [
            'id' => 34,
            'title' => 'Title ajaxxxx ',
            'description' => 'This row AJAXSECONDdddd',
            'price' => '99.44',

        ];

        return new JsonModel($object);
    }

    public function formAction()
    {

        $username = new Element\Text('username');
        $username
            ->setLabel('Username')
            ->setAttributes(array(
                'class' => 'username',
                'size'  => '20',
            ));

        $password = new Element\Password('password');
        $password
            ->setLabel('Password')
            ->setAttributes(array(
                'size'  => '15',
            ));

        $button = new Element\Button('button');
        $button->setLabel('My Button')
               ->setValue('Send Form');


        $captcha = new Element\Captcha('captcha');
        $captcha
            ->setCaptcha(new Captcha\Dumb())
            ->setLabel('Please verify you are human');


        $checkbox = new Element\Checkbox('checkbox');
        $checkbox->setLabel('Select a checkbox');
        $checkbox->setUseHiddenElement(true);
        $checkbox->setCheckedValue("good");
        $checkbox->setUncheckedValue("bad");

        $multiCheckbox = new Element\MultiCheckbox('multi-checkbox');
        $multiCheckbox->setLabel('What do you like  Pes ?');
        $multiCheckbox->setValueOptions(array(
            '0' => 'Apple',
            '1' => 'Orange',
            '2' => 'Lemon'
        ));

        //? под вопросом для чего
        $colors = new Element\Collection('collection');
        $colors->setLabel('Colorss');
        $colors->setCount(3);
        $colors->setTargetElement(new Element\Color());
        $colors->setShouldCreateTemplate(true);

        // тоже не понял для чего зто
        $csrf = new Element\Csrf('csrf');



        $file = new Element\File('file');
        $file->setLabel('Single file input');

            // HTML5 multiple file upload

        $multiFile = new Element\File('multi-file');
        $multiFile->setLabel('Multi file input')
            ->setAttribute('multiple', true);

        $hidden = new Element\Hidden('hidden');
        $hidden->setValue('foo hidden Ninja ');

        $image = new Element\Image('image');
        $image->setAttribute('src','/img/pexel.jpeg'); // Src attribute is required


        $monthYear = new Element\MonthSelect('monthyear');
        $monthYear->setLabel('Select a month and a year');
        $monthYear->setMinYear(1984);

        $radio = new Element\Radio('gender');
        $radio->setLabel('What is your gender ?');
        $radio->setValueOptions(array(
            '0' => 'Female',
            '1' => 'Male',
        ));

        $select = new Element\Select('language');
        $select->setLabel('Which is your mother tongue?');
        $select->setValueOptions(array(
            '0' => 'French',
            '1' => 'English',
            '2' => 'Japanese',
            '3' => 'Chinese',
            '4' => 'Moldavian',
            '5' => 'Zimbabwian',
        ));

        $textarea = new Element\Textarea('textarea');
        $textarea->setLabel('Enter a description');

        $submit = new Element\Submit('submit');
        $submit->setValue('Submit this Form');

        $date = new Element\Date('appointment-date');
        $date
            ->setLabel('Appointment Date')
            ->setAttributes(array(
                'min'  => '2007-01-01',
                'max'  => '2020-01-01',
                'step' => '3', // days; default step interval is 1 day
            ))
            ->setOptions(array(
                'format' => 'd-m-Y'
            ));

        $number = new Element\Number('quantity');
        $number
            ->setLabel('Quantity')
            ->setAttributes(array(
                'min'  => '0',
                'max'  => '21',
                'step' => '3', // default step interval is 1
            ));

        $email = new Element\Email('email');
        $email->setLabel('Email Address');


        // Comma separated list of emails
        $emails = new Element\Email('emails');
        $emails
            ->setLabel('Email Addresses')
            ->setAttribute('multiple', true);

        $range = new Element\Range('range');
        $range
            ->setLabel('Minimum and Maximum Amount')
            ->setAttributes(array(
                'min'  => '0',   // default minimum is 0
                'max'  => '1000', // default maximum is 100
                'step' => '20',   // default interval is 20
            ));

        $color = new Element\Color('color');
        $color->setLabel(' Select a Background color');

        $url = new Element\Url('webpage-url');
        $url->setLabel('Webpage URL');

        $form = new Form('form');
        $form
            ->add($username)
            ->add($password)
            ->add($button)
            ->add($captcha)
            ->add($checkbox)
            ->add($colors)
            ->add($csrf)
            ->add($file)
            ->add($multiFile)
            ->add($hidden)
            ->add($image)
            ->add($monthYear)
            ->add($radio)
            ->add($select)
            ->add($textarea)
            ->add($submit)
            ->add($date)
            ->add($number)
            ->add($email)
            ->add($emails)
            ->add($range)
            ->add($color)
            ->add($url)
            ->add($multiCheckbox);



        return array('form' => $form);

    }
}