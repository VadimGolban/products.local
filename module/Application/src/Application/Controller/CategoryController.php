<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Entity\Category;
use Application\Form\CategoryForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CategoryController extends AbstractActionController
{
    public function createAction()
    {
        $form = new CategoryForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $category = new Category();
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = $form->getData();

                $category->setTitle($data['title']);

                /** @var \Doctrine\ORM\EntityManager $em */
                $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

                $em->persist($category);
                $em->flush();

                return $this->redirect()->toRoute('categorys', ['action' => 'index']);

            }
        }

        return array('form' => $form);
    }

    public function indexAction()
    {
        /** @var EntityManager $em */
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $categories = $em->getRepository('Application\Entity\Category')->findAll();

        return new ViewModel(['categories' => $categories]);
    }

    public function deleteAction()
    {
        /** @var Category $deleteCategory */
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $deleteCategory = $em->getRepository('Application\Entity\Category')->find($productId = $this->params()->fromQuery('id', 0));
        if ($deleteCategory ) {
            $em->remove($deleteCategory);
            $em->flush();
        }

        return $this->redirect()->toRoute('categorys', ['action' => 'index']);
    }

    public function editAction()
    {
        /** @var Category $category */
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $category = $em->getRepository('Application\Entity\Category')->find($productId = $this->params()->fromQuery('id', 0));

        $form = new CategoryForm();
        $request = $this->getRequest();

        $form->populateValues($category->toArray());

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = $form->getData();

                $category->setTitle($data['title']);

                $em->persist($category);
                $em->flush();

                return $this->redirect()->toRoute('categorys', ['action' => 'index']);
            }
        }

        return array('form' => $form);
    }
}