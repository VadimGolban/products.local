<?php


namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class RedController extends  AbstractActionController
{
    public function orangeAction(){

        $viewModel = new ViewModel();
        $viewModel->setTemplate('category/orange');

        return $viewModel;
    }
}