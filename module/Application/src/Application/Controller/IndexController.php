<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Entity\Category;
use Application\Form\ProductFilterForm;
use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        /** @var EntityManager $em */
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $productsQB = $em->getRepository('Application\Entity\Product')->createQueryBuilder('p');

        /** @var \Doctrine\ORM\EntityManager $em */
        /** @var Category $categories */
        $categories = $em->getRepository('Application\Entity\Category')->findAll();

        $form = new ProductFilterForm($categories);
        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $productsQB
                    ->where('p.category = :category')
                    ->setParameter('category', $form->get('category')->getValue());
            }
        }

        $products = $productsQB->getQuery()->execute();

        return array('form' => $form, 'products' => $products);
   }


    public function ajaxAction()
    {
        $object1 = [
            'id' => 7,
            'title' => 'Title',
            'description' => 'This row added via AJAX',
            'category' => 'Bikess',
            'price' => '101.23',
        ];

        return new JsonModel($object1);
    }

    public function ajaxTwoAction()
    {
        $object = [
            'id' => 9,
            'title' => 'Title AJAX2',
            'description' => 'This row added via AJAX2',
            'category' => 'Skate',
            'price' => '1045.33',
        ];

        return new JsonModel($object);
    }
}
