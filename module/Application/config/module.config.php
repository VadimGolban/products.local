<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

return array(
    'doctrine' => array(
        'driver' => array(
            // defines an annotation driver with two paths, and names it `my_annotation_driver`
            'application_entity' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/Application/Entity',
                ),
            ),

            // default metadata driver, aggregates all other drivers into a single one.
            // Override `orm_default` only if you know what you're doing
            'orm_default' => array(
                'drivers' => array(
                    // register `my_annotation_driver` for any entity under namespace `My\Namespace`
                    'Application\Entity' => 'application_entity'
                ),
            ),
        ),
    ),

    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index'  => 'Application\Controller\IndexController',
            'Application\Controller\Product'  => 'Application\Controller\ProductController',
            'Application\Controller\Category'  => 'Application\Controller\CategoryController',
            'Application\Controller\Forms'  => 'Application\Controller\FormsController',
            'Application\Controller\Red'  => 'Application\Controller\RedController',
        ),
    ),

    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'ajax' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/ajax',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'ajax',
                    ),
                ),
            ),
            'ajaxForm' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/ajaxSecond',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Forms',
                        'action'     => 'ajaxSecond',
                    ),
                ),
            ),
            'ajaxTwo' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/ajaxTwo',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'ajaxTwo',
                    ),
                ),
            ),
            'index' => array(
               'type' => 'Segment',
               'options' => array(
                   'route'    => '/index[/:action]',
                   'defaults' => array(
                      'controller' => 'Application\Controller\Index',
                       'action'     => 'index',
                   ),
                   'constraints' => array(
                       'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                   ),
               ),
            ),
            'products' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/admin/product[/:action]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Product',
                        'action'     => 'create',
                    ),
                    'constraints' => array(
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                ),
                'may_terminate' => true
            ),
            'categorys' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/admin/category[/:action]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Category',
                        'action'     => 'create',
                    ),
                    'constraints' => array(
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                ),
                'may_terminate' => true
            ),
            'formes' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/forms[/:action]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Forms',
                        'action'     => 'form',
                    ),
                    'constraints' => array(
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                ),
                'may_terminate' => true
            ),
            'red' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/category[/:action]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Red',
                        'action'     => 'index',
                    ),
                    'constraints' => array(
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                ),
                'may_terminate' => true
            ),
        ),

    ),


    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories' => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),

    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'category/orange'         => __DIR__ . '/../view/application/category/orange.phtml',
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
